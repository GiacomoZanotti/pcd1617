package pcd.ass02.ex1;

public class GameFinishedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Result result;
	
	
	public GameFinishedException(String message,Result result){
		super(message);
		this.result=result;
		
	}
	
	public Result getLastResult(){
		return result;
	}
	
	
	
	
	

}
