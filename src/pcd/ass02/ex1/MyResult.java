package pcd.ass02.ex1;

public class MyResult implements Result {

	private long guess;
	private long secretnumber;

	public MyResult(long guess, long secretnumber) {
		this.guess = guess;
		this.secretnumber=secretnumber;
	}

	@Override
	public boolean found() {
		// TODO Auto-generated method stub
		return secretnumber == guess;
	}

	@Override
	public boolean isGreater() {
		// TODO Auto-generated method stub
		return secretnumber < guess;
	}

	@Override
	public boolean isLess() {
		// TODO Auto-generated method stub
		return secretnumber > guess;
	}
	
	@Override
	public String toString(){
		return "this is the guess: "+this.guess+" and this is the secret number "+secretnumber;
	}

}
