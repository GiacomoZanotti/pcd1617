package pcd.ass02.ex2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MandelbrosConcurrentSetImage implements MandelbrotSetImage {

	private int width, height;
	private int image[];
	private Complex center;
	private double delta;

	public MandelbrosConcurrentSetImage(int w, int h, Complex center, double delta) {
		this.width = w;
		this.height = h;
		this.image = new int[w * h];
		this.center = center;
		this.delta = delta;

	}

	@Override
	public void compute(int nIterMax) {
		// TODO Auto-generated method stub
		int fromx = 0;
		

		ExecutorService executorService = Executors.newFixedThreadPool(10);
		int tasks = 23;

		int range = width / tasks;
		int tox=range;
		int difference=width%tasks;
		
		for (int i = 0; i < tasks; i++) {
			executorService.execute(new Worker(fromx, tox, this, nIterMax));
			fromx=tox;
			tox+=range;
		}
		if(difference!=0){
		executorService.execute(new Worker(fromx,this.width,this,nIterMax));
		
		}
		executorService.shutdown();
		try {
			executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void computeSubImage(int fromx, int tox, int nIterations) {
		for (int x = fromx; x < tox; x++) {
			for (int y = 0; y < height; y++) {
				Complex c = getPoint(x, y);
				double level = computeColor(c, nIterations);
				int color = (int) (level * 255);
				int pixelPosition = y * width + x;
				int value = color + (color << 8) + (color << 16);
				image[pixelPosition] = value;

			}
		}

	}

	private double computeColor(Complex c, int maxIteration) {

		int iteration = 0;
		Complex z = new Complex(0, 0);

		/*
		 * Repeatedly compute z := z^2 + c until either the point is out of the
		 * 2-radius circle or the number of iteration achieved the max value
		 * 
		 */
		while (z.absFast() <= 2 && iteration < maxIteration) {
			z = z.times(z).plus(c);
			iteration++;
		}
		if (iteration == maxIteration) {
			/* the point belongs to the set */
			return 0;
		} else {
			/* the point does not belong to the set => distance */
			return 1.0 - ((double) iteration) / maxIteration;
		}
	}

	@Override
	public Complex getPoint(int x, int y) {
		// TODO Auto-generated method stub
		return new Complex((x - width * 0.5) * delta + center.re(), center.im() - (y - height * 0.5) * delta);

	}

	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return height;
	}

	@Override
	public int getWidth() {
		// TODO Auto-generated method stub
		return width;
	}

	@Override
	public int[] getImage() {
		// TODO Auto-generated method stub
		return image;
	}

	@Override
	public void updateRadius(double radius) {
		// TODO Auto-generated method stub
		delta = radius / (width * 0.5);

	}

}
