package pcd.ass02.ex2;

public class Worker extends Thread {
	private int fromX, toX, nIterations;
	private MandelbrosConcurrentSetImage mandelbrotSet;

	public Worker(int fromX, int toX, MandelbrosConcurrentSetImage mandelbrotSet, int nIterations) {
		super();
		this.fromX = fromX;
		this.toX = toX;

		this.nIterations = nIterations;
		this.mandelbrotSet = mandelbrotSet;

	}

	public void run() {
		mandelbrotSet.computeSubImage(fromX, toX, nIterations);

	}

}
