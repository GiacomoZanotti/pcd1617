package pcd.ass03.ex1;

public interface Result {
	
	boolean found();
	boolean isGreater();	
	boolean isLess();
	

}
