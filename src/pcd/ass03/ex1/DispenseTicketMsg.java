package pcd.ass03.ex1;


import akka.actor.ActorRef;

public class DispenseTicketMsg {

	private ActorRef oracle;

	public DispenseTicketMsg(ActorRef oracle) {
		super();
		this.oracle = oracle;
	}

	public ActorRef getOracle() {
		return oracle;
	}



}
