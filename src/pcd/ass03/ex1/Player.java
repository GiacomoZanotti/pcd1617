package pcd.ass03.ex1;

import java.util.Random;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;

public class Player extends UntypedActor {

	private long guess, min, max;
	private Random random;
	private ActorRef dispenser;

	public Player(ActorRef dispenser) {
		this.dispenser = dispenser;
		guess = -1;
		min = 0;
		max = Long.MAX_VALUE;
		random = new Random();
	}

	public static Props create(ActorRef dispenser) {
		return Props.create(Player.class, () -> new Player(dispenser));
	}

	
	@Override
	public void onReceive(Object message) throws Throwable {

		// TODO Auto-generated method stub

		if (message instanceof TicketMsg) {
			TicketMsg ticket = (TicketMsg) message;
			guess = min + (max - min > 0 ? Math.abs(random.nextLong()) % (max - min) : max);
			getSender().tell(new GuessingNumberMsg(guess, ticket, this.getSelf()), getSelf());
		}
		if (message instanceof ResultMsg) {
			ResultMsg rmsg = (ResultMsg) message;
			
			if (rmsg.isGreater()) {
				min = guess;

			}
			if (rmsg.isLess()) {

				max = guess;
			}
			dispenser.tell(new DispenseTicketMsg(getSender()), getSelf());

		}
		if(message instanceof GameOver){
			GameOver gameover=(GameOver)message;
			log(gameover.isFound()?"WON! "+guess:"SOB!");
			getContext().stop(getSelf());
			dispenser.tell(akka.actor.PoisonPill.getInstance(), ActorRef.noSender());
		}

	}

	private void log(String string) {
		// TODO Auto-generated method stub
		System.out.println(this.getSelf().path().name() + " " + string);

	}

}
