package pcd.ass03.ex1;

import akka.actor.ActorRef;

public class GuessingNumberMsg implements Comparable<GuessingNumberMsg>{

	private long guess;
	private TicketMsg ticket;
	private ActorRef player;

	public GuessingNumberMsg(long guess, TicketMsg ticket, ActorRef player) {
		this.guess = guess;
		this.ticket = ticket;
		this.player=player;
		
	}
	

	public ActorRef getPlayer() {
		return player;
	}


	public long getGuess() {
		return guess;
	}

	public TicketMsg getTicket() {
		return ticket;
	}
	
	public int getTicketNumber(){
		return ticket.getTicketNumber();
	}

	public String toString(){
		return "I'm player "+this.player.path()+", with guess "+guess +", and"+ticket.toString();
				
	}


	@Override
	public int compareTo(GuessingNumberMsg o) {
		// TODO Auto-generated method stub
		if(this.getTicketNumber()>o.getTicketNumber()){
			return 1;
		}
		if(this.getTicketNumber()<o.getTicketNumber()){
			return -1;
		}
		else{
			return 0;
		}
	}

}
