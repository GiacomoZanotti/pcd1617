package pcd.ass03.ex1;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class OracleView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton secretNumberGenerator, start, stop, reset;
	private JTextField textSecretNumber, textNumberOfPlayers;
	private JTextArea textArea;
	private Controller controller;
	private Dimension buttonsDimension = new Dimension(150, 27);

	public OracleView(Controller controller) {
		super("Guess The Magic Number");
		this.controller = controller;
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(800, 400);

		InitGui();

	}

	private void InitGui() {

		JPanel panel1 = new JPanel(new FlowLayout());
		JPanel panel2 = new JPanel(new FlowLayout());
		JPanel panel3 = new JPanel(new FlowLayout());

		secretNumberGenerator = new JButton("create the number");
		start = new JButton("start");
		stop = new JButton("stop");
		reset = new JButton("reset");
		start.setPreferredSize(buttonsDimension);
		stop.setPreferredSize(buttonsDimension);
		reset.setPreferredSize(buttonsDimension);
		secretNumberGenerator.setPreferredSize(buttonsDimension);
		textArea = new JTextArea(400, 200);
		textArea.setEditable(false);
		textSecretNumber = new JTextField("use the button to create a random number or type one");
		textSecretNumber.setEditable(true);
		textNumberOfPlayers = new JTextField("type number of players");
		textNumberOfPlayers.setEditable(true);
		secretNumberGenerator.addActionListener(l -> this.controller.createSecretNumber(textSecretNumber));
		start.addActionListener(l -> this.controller.start(l,textNumberOfPlayers, textSecretNumber));
		stop.addActionListener(l -> this.controller.stop());
		reset.addActionListener(l -> this.controller.reset(textNumberOfPlayers, textSecretNumber));
		panel1.add(secretNumberGenerator);
		panel1.add(textSecretNumber);
		panel1.add(textNumberOfPlayers);
		add(panel1, BorderLayout.NORTH);
		panel2.add(start);
		panel2.add(stop);
		panel2.add(reset);
		add(panel2, BorderLayout.SOUTH);
		panel3.add(textArea);
		add(panel3, BorderLayout.CENTER);

	}

}
