package pcd.ass03.ex1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;

public class TicketDispenser extends UntypedActor {

	private List<ActorRef> players = new ArrayList<>();
	private List<TicketMsg> tickets = new ArrayList<>();
	private int numberOfPlayers;

	public TicketDispenser(int numberOfPlayers) {
		this.numberOfPlayers = numberOfPlayers;
		for (int i = 0; i < numberOfPlayers; i++) {
			tickets.add(new TicketMsg(i+1));
		}
	}

	public static Props create(int numberOfPlayers) {
		return Props.create(TicketDispenser.class, () -> new TicketDispenser(numberOfPlayers));
	}


	@Override
	public void onReceive(Object message) throws Throwable {
		// TODO Auto-generated method stub

		if (message instanceof DispenseTicketMsg) {
			players.add(getSender());
			//log(getSender().path().name()); //to check if players are served according to a FIFO policy

			DispenseTicketMsg msg = (DispenseTicketMsg) message;

			if (players.size() == numberOfPlayers) {
				Collections.shuffle(tickets);
				for(int i=0;i<numberOfPlayers;i++){
					players.get(i).tell(tickets.get(i), msg.getOracle());
				}
				players.clear();

			}
		}

	}

	@SuppressWarnings("unused")
	private void log(String string) {
		// TODO Auto-generated method stub
		System.out.println(this.getClass().getName() + " " + string);

	}

}
