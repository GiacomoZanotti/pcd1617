package pcd.ass03.ex1;

public class TicketMsg {

	private int ticketNumber;

	public TicketMsg(int ticketNumber) {
		super();
		this.ticketNumber = ticketNumber;
	}

	public int getTicketNumber() {
		return ticketNumber;
	};

	public String toString(){
		return " ticket of number "+ticketNumber;
				
	}
	
}
