package pcd.ass01.ex2;


public interface Controller  {
	

	void action(String event);
	
	void start();
	

}
