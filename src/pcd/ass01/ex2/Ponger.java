package pcd.ass01.ex2;

import java.util.concurrent.Semaphore;

public class Ponger extends PingerPonger {
	private Semaphore semaphore2;
	private Semaphore semaphore3;

	public Ponger(Counter counter, Semaphore semaphore2, Semaphore semaphore3) {
		super(counter);
		// TODO Auto-generated constructor stub
		this.semaphore2 = semaphore2;
		this.semaphore3 = semaphore3;
	}

	@Override
	protected void action() {
		// TODO Auto-generated method stub
		try {
			this.semaphore3.acquire(2);
			//if (!shutdown) {

				System.out.println("Pong!");
				this.counter.update();
				Thread.sleep(PAUSE);

				this.semaphore2.release();

			//}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	protected String getPingerPongerName() {
		// TODO Auto-generated method stub
		return "Ponger is saying";
	}

}
