package pcd.ass01.ex2;

public interface CounterObserver {

	void counterUpdated(Counter counter);
}
