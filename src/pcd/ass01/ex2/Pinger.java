package pcd.ass01.ex2;

import java.util.concurrent.Semaphore;

public class Pinger extends PingerPonger {
	private Semaphore semaphore1, semaphore2;

	public Pinger(Counter counter, Semaphore semaphore1, Semaphore semaphore2) {
		super(counter);
		this.semaphore1=semaphore1;
		this.semaphore2=semaphore2;

	}

	@Override
	protected void action() {

		try {
			this.semaphore1.acquire(2);
			if (!off) {

				System.out.println("Ping!");
				counter.update();
				Thread.sleep(PAUSE);
				this.semaphore2.release();
				
			}

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected String getPingerPongerName() {
		// TODO Auto-generated method stub
		return "Pinger is saying";
	}

}
