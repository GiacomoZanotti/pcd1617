package pcd.ass01.ex2;


public abstract class PingerPonger extends Thread {

	protected Counter counter;
	protected boolean off;
	protected static final int PAUSE = 0;

	public PingerPonger(Counter counter) {
		
		this.counter = counter;
		this.off = false;
	}

	public void run() {
		while (true) {
		
				try {
					this.action();
					
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();

				
			}
		}
	}

	public void switchOff() {
		off = true;
		System.out.println(this.getPingerPongerName() + " Goodbye!");
	}

	protected abstract void action();

	protected abstract String getPingerPongerName();

}
