package pcd.ass01.ex1;

/**
 * Class for computing an image of a region
 * of the Mandelbrot set.
 * 
 * @author aricci
 *
 */
public class MandelbrotSetImageImpl implements MandelbrotSetImage {
	
	private int width,height;
	private int image[]; 
	private Complex centerComplexNumber;
	private double delta;
	
		
	/**
	 * Creating an empty Mandelbrot set image. 
	 * 
	 * @param width width in pixels
	 * @param height height in pixels
	 * @param centerComplexNumber center of the Mandelbrot set to be represented
	 * @param radius radius of the Mandelbrot set to be represented
	 */
	public MandelbrotSetImageImpl(int width, int height, Complex centerComplexNumber, double radius){
		this.width = width;
		this.height = height;
        this.image = new int[width*height];
        this.centerComplexNumber = centerComplexNumber;
        this.delta = radius/(width*0.5);
	}
	
	/**
	 * 
	 * Compute the image with the specified level of detail
	 * 
	 * See https://en.wikipedia.org/wiki/Mandelbrot_set
	 * 
	 * @param nIterMax number of iteration representing the level of detail
	 */
	public void compute(int nIterMax){	
		
		/*
		 * for each pixel of the image
		 * - get the corresponding point on the complex plan
		 * - verify if the point either belongs or not to the Mandelbrot set
		 * -- yes => black color 
		 * -- no => level of gray, depending on the distance from the set
		 */
		for (int x = 0; x < width; x++ ){
			for (int y = 0; y < height; y++){
				Complex c = getPoint(x,y);
				double level = computeColor(c,nIterMax);
				int color = (int)(level*255);
				int pixelColored=y*width+x;
				image[pixelColored] = color + (color << 8)+ (color << 16);
			}
		}
	}
		
	/**
	 * Basic Mandelbrot set algorithm: returns 0 (color black) if the point belongs to the set, otherwise its distance (defining the grey color)
	 *  
	 * @param c
	 * @param maxIteration
	 * @return
	 */
	private double computeColor(Complex c, int maxIteration){
		int iteration = 0;		
		Complex z = new Complex(0,0);

		/*
		 * Repeatedly compute z := z^2 + c
		 * until either the point is out 
		 * of the 2-radius circle or the number
		 * of iteration achieved the max value
		 * 
		 */
		while ( z.absFast() <= 2 &&  iteration < maxIteration ){ //calcola il modulo del numero complesso
			z = z.times(z).plus(c); 
			iteration++;
		  }		 
		  if ( iteration == maxIteration ){			  
			  /* the point belongs to the set*/
			  return 0;
		  } else {
			  /* the point does not belong to the set => distance */
			  return 1.0-((double)iteration)/maxIteration;
		  }
	}
	
	/**
	 * This method returns the point in the complex plane
	 * corresponding to the specified element/pixel in the image
	 * 
	 * @param x x coordinate in the image
	 * @param y y coordinate in the image
	 * @return the corresponding complex point
	 */
	public Complex getPoint(int x, int y){
		return new Complex((x - width*0.5)*delta + centerComplexNumber.re(), centerComplexNumber.im() - (y - height*0.5)*delta); 
	}
	
	/**
	 * Get the height of the image
	 * @return
	 */
	public int getHeight(){
		return height;
	}

	/**
	 * Get the width of the image
	 * @return
	 */
	public int getWidth(){
		return width;
	}
	
	/**
	 * Get the image as an array of int, organized per rows
	 * (compatible with the BufferedImage style)
	 * 
	 * @return
	 */
	public int[] getImage(){
		return image;
	}
	
	

}

