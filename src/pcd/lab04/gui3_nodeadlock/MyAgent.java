package pcd.lab04.gui3_nodeadlock;

public class MyAgent extends Thread {

	private MyModel model;
	
	public MyAgent(MyModel model){
		this.model = model;
	}
	
	public void run(){
		while (true){
			try {
				System.out.println("Agent is working now!");
				model.update();
				Thread.sleep(500);
			} catch (Exception ex){
			}
		}
	}
}
