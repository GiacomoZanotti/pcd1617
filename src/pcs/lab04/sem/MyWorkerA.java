package pcs.lab04.sem;

import java.util.concurrent.Semaphore;

public class MyWorkerA extends Worker {

	private Semaphore mutex;

	public MyWorkerA(String name, Semaphore mutex) {
		super(name);
		this.mutex = mutex;
	}

	public void run() {
		while (true) {
		//	action1();
			try {
				mutex.acquire();
				action2();
				action3();
				mutex.release();
				Thread.sleep(1500);
			} catch (InterruptedException ex) {
				break;
			}
		}
	}

	protected void action1() {
		println(this.getName()+" aa");
//		wasteRandomTime(100, 500);
	}

	protected void action2() {
		println(this.getName()+" bb");
	//	wasteRandomTime(300, 700);
	}

	protected void action3() {
		println(this.getName()+" cc");
		wasteRandomTime(300, 700);
	}
}
